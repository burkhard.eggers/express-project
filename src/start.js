var express = require('express');
var path = require('path');
var images = require('./images');

var app = express();

var publicDir = path.join(__dirname, '/../public');
console.log(publicDir);
app.use(express.static(publicDir));
app.set('views', './views');
app.set('view engine', 'pug');

app.get('/', (req, res) => res.render('index'));

app.get('/view1', (req, res) => res.render('view', { images }));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});